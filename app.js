
/**
 * Module dependencies.
 */

var express = require('express')
  , config = require('./config/config.js')
  , https = require('https')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
  , cookieParser = require('cookie-parser')
  , session = require('express-session')
  , mongoStore = require('connect-mongo')({session: session})
  , fs = require('fs');

var conn = mongoose.connect(config.databaseUrl); // MongoDb Connection
var app = express(); // Application

// COnfigurazione della directory con file statici
app.use(express.static(path.join(__dirname, 'public')));

// Viene caricato il certificato per HTTPS
var privateKey  = fs.readFileSync('server.pem', 'utf8');
var certificate = fs.readFileSync('server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

// all environments
//app.set('port', process.env.PORT || 3000);
app.set('portHttps', process.env.PORT || 8443); // Settaggio per la porta da usare
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// Imposta le configurazioni per il token dal file config.js
app.set('secretforToken', config.secretforToken); // secret variable for Api token generating
app.set('tokenExpTime', config.tokenExpTime);

// Settings per Express
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(cookieParser());
app.use(express.methodOverride());

//Setting per la session
app.use(session({
	path: '/',
	secret: config.secretforSession, // Parola segreta per il cookie
	cookie: {maxAge: 60*60*1000}, // Data di scadenza cookie
	store: new mongoStore({
		mongooseConnection: mongoose.connection, // La sessione viene memorizzata sul DB Mongo
		collections: 'sessions' // La collection sarà chamata 'sessions'
	}),
	resave : true,
	saveUninitialized: true
}));

//Setting for development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
  app.configure('development', function () { app.locals.pretty = true; });
}

//Setting for Routes
require('./config/routes.js')(app, express);

// Avoid application crash and logs errors in console
process.on('uncaughtException', function(err) {
    console.log(err);
});

//Close connection to mongodb if application crash
process.on('SIGINT', function() {  
	  mongoose.connection.close(function () { 
	    console.log('Mongoose default connection disconnected through app termination'); 
	    process.exit(0); 
	  }); 
	}); 

// Crea il server per l'applicazione
https.createServer(credentials, app).listen(app.get('portHttps'), function(){
  console.log('Express server listening on port ' + app.get('portHttps'));
  console.log('Mongodb connection readyState: ' + mongoose.connection.readyState);
  //var userModel = new UserModel({username:'ciao'});
  //console.log(userModel);
});

/*http.createServer(app).listen(app.get('port'), function(){
	  console.log('Express server listening on port ' + app.get('port'));
	  console.log('Mongodb connection readyState: ' + mongoose.connection.readyState);
});*/
