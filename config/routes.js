/**
 * Routes.js
 */
// Carico il modello (Db Schema) dello User
var UserModel = require('../models/users_model.js');
//Carico il modulo per la gestione del token di sicurezza
var jwt = require('jsonwebtoken');
// Carico i controllers per l'applicazione e per le API 
var UsersApiController = require('../controllers/users_api_controller.js');
var appController = require('../controllers/app_controller.js');

module.exports = function(app, express) {
	
	// Visulizza la homepage se nessun utente è loggato, la dashboard se l'utente è loggato
	app.get('/', appController.renderRootPage(app));
	
	// Visulizza la pagina di registrazione
	app.get('/registration', appController.renderRegistration);
	// Visulizza la pagina di login
	app.get('/login', appController.renderLogin);
	
	// Effettua il logout
	app.get('/logout', appController.renderLogout);
	
	//Settaggi per le API
	app.all('/api/*', function(req, res, next) {
		  // CORS headers
		  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
		  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		  // Set custom headers for CORS
		  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
		  if (req.method == 'OPTIONS') {
		    res.status(200).end();
		  } else {
		    next();
		  }
		});

	//API
	// Authenticate prende in input nome utente e password e restituisce un token: input[email, password], output[token]
	app.post('/api/authenticate', UsersApiController.authenticate(app));
	// Register prende in input nome utente e password e restituisce success=true nel caso la registrazione sia avvenuta con successo:
	//input[email, password], output[success, message]
	app.post('/api/register', UsersApiController.register);
	
	//API con autorizzazioni, queste API sono protette dalla funzione middlewareTokenFunction che verifica se il token passato sia valido
	// Viene controllato se il token è presente in request, e se valido viene eseguita la funzione
	
	// Api Delete per la cancellazione di un utente, accesso riservato
	app.delete('/api/deleteUser/:email', [UsersApiController.middlewareTokenFunction(app), UsersApiController.deleteUser]);
	// Api Delete per la modifica di un utente, accesso riservato
	app.put('/api/updateUser/:email', [UsersApiController.middlewareTokenFunction(app),  UsersApiController.updateUser]);
	// Api per la visualizzazione di tutti gli utenti registrati nel sistema con relative informazioni, accesso riservato
	app.get('/api/users', [UsersApiController.middlewareTokenFunction(app), UsersApiController.listUsers]);

};
