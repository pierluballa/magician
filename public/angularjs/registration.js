/**
 * Registration Angular js file
 */
//Crea il modulo AngularJs per la registrazione
var registrationApp = angular.module('registrationApp', []);

//Crea il controller AngularJs per la registrazione
registrationApp.controller('registrationController', function($scope, $http) {
	$scope.name = 'Magician name';
	
	$scope.registrationSuccess = false;

	// Funzione che fa il submit del form per la registrazione dopo aver accertato la validità dello stesso            
	$scope.submitForm = function(isValid, user) {
		console.log(user);
		// Controlla se il form è valido
		if (isValid) {
			
			// Prepara l'invio dati alla chiamata post al servizio
			var formData = {
					email: user.email,
					password: user.password,
					name: user.name,
					address: user.address,
					isNewsletterSignup: user.isNewsletterSignup==true?true:false,
					isSendNotification: user.isSendNotification==true?true:false
			}; 

			// Chiamata RESTFUL per la creazione dell'utente
			$http.post('/api/register', formData).success(function(data) {
				alert(data.message);
				if (data.success == true)
					$scope.registrationSuccess = true;
				console.log(data);
			}).error(function(data) {
				alert("Undefined error in registration, plaese try again!")
				console.log('Error: ' + data);
			});

		}

	};

});