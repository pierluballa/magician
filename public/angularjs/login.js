/**
 * Login Angular js file
 */
var loginApp = angular.module('loginApp', []);

//create angular controller
loginApp.controller('loginController', function($scope, $http, $window) {
	$scope.loginError = false;
	$scope.loginErrorMsg = "";

	// function to submit the form after all validation has occurred            
	$scope.submitForm = function(isValid, user) {

		// check to make sure the form is completely valid
		if (isValid) {
			
			var formData = {
					email: user.email,
					password: user.password
			}; 
			console.log("formData: "+ formData);

			// API call to create User
			$http.post('/api/authenticate', formData).success(function(data) {
				
				if (data.success==false){
					$scope.loginError = true;
					$scope.loginErrorMsg = data.message;
				}
				else {
					$window.location.href = '/'; // Utente loggato, viene reindirizzato alla dashboard
				}
					
				
			}).error(function(data) {
				alert("Undefined error in login, plaese try again!")
				console.log('Error: ' + data);
			});

		}

	};

});