/**
 * Users Model
 */
var mongoose = require('mongoose'); // Libreria per la gestione dello Schema User
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt'); // Libreria per hash della password 

var UserSchema = new Schema({
	email: {type: String, required: true, unique: true},
	password: {type: String, required: true},
	name: {type: String},
	address: {type: String},
	isNewsletterSignup: {type: Boolean},
	isSendNotification: {type: Boolean}
});

//Metodi ======================
//Genera l'hash della password
UserSchema.methods.generateHash = function(password) {
 return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//Controlla se la hashed password è valida
UserSchema.methods.validPassword = function(password, hash_password) {
 return bcrypt.compareSync(password, hash_password);
};

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;
