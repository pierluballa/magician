# Magician App
## Description
Magitian app is a user management app with exposed REST API,
developed using the MEAN stack: Node.js + Express + MongoDB + Mongoose + Jade + Angular.js + Moongose

## Installazione

```sh
$ git clone https://pierluballa@bitbucket.org/pierluballa/magician.git magician
$ cd magician
$ node app.js
```
per l'avvio in produzione usare:
```NODE_ENV=production node app
```

## Uso
- Nel browser aprire la pagina https://localhost:8443
- Registrare un utente, cliccando su Register oppure andare all'indirizzo https://localhost:8443/register
- Effettuare il login con le credenziali create cliccando su "Login" oppure andando all'indirizzo https://localhost:8443/login
- Dopo aver effettuato il login si verrà reindirizzati verso la dashboard che contiene le informazioni degli utenti registrati

##La struttura
- Il file config/config.js contiene le configurazioni (link al db, durata sessione, e secret per le sessioni e token)
- Il file config/routes.js contiene gli url e i settaggi per le routes
- Secondo il pattern MVC, esistono 3 cartelle: "models" per i modelli, "views" per il rendering delle pagine e "controllers" per la logica implementativa
- app.js contiene i settaggi principali per l'applicazione

#Descrizione dell'implementazione
##Tecnologie utilizzate
### FrontEnd
- BootStrap: per il css e lo stile della web application
- Angular.js: per la validazione dei form, chiamate alle API e generazione html (ad esempio per la generazione della tabella degli utenti con ng-repeat)
- Jade: come template engine
### Backend
- Node.js e Express: implementazione dell'applicazione, implementazione API e meccanismi di sicurezza
- MongoDB e Mongoose per la gestione della sessione e dei dati persistenti degli utenti
## Dettagli implementativi
> Per la registrazione dell'utente è necessario fornire email e password di almeno 8 caratteri; l'email identifica univocamente l'utente, altre informazioni aggiuntive di esempio sono richieste al login ma non obbligatorie.
## RESTFull API 
### Operazioni di accesso al sistema
- "api/register": endpoint che prende in input email e password e ritorna success=true in caso di avvenuta registrazione, success=false altrimenti (inserisce l'utente nel sistema - POST method)
- "api/authenticate": endpoint che prende in input email e password e ritorna success=true e un token in caso di avvenuta autenticazione, success=false altrimenti
### Operazioni CRUD per gli utenti registrati al sistema
- "api/deleteUser/:email": endpoint che prende in input email, token di auteticazione e ritorna success=true in caso di avvenuta eliminazione dell'utente, success=false altrimenti (DELETE method)
- "api/updateUser/:email": endpoint che prende in input email, token di auteticazione e i campi dell'utente che si vuole modificare{address, password, name...} ritorna success=true in caso di avvenuto aggiornamento dei dati dell'utente, success=false altrimenti (PUT method)
- "api/users": endpoint che prende in input il solo token e restituisce success=true con la lista completa degli utenti registrati nel sistema, success=false altrimenti (GET method)
## Sicurezza
> Per la gestione della sicurezza, oltre all'utilizzo di https, quindi crittografia di tipo Transport Layer Security (SSL/TLS) (Al momento è impostato un certificato di prova che è possibile cambiare), ho utilizzato anche un meccanismo di protezione per le chiamate CRUD al sistema RestFull: implementando un token di sicurezza che evita l'invio delle credenziali. Tale token è memorizzato in sessione una volta generato attraverso l'endpoint "/api/authenticate". Il token permette che le chiamate successive al login siano autenticate.