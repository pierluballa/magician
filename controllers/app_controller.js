/**
 * App controller, gestisce il render delle pagine principali
 */
var jwt = require('jsonwebtoken'); //Carico il modulo per la gestione del token di sicurezza
var UserModel = require('../models/users_model.js'); // Carico il modello (Db Schema) dello User

module.exports = {
		
	renderRootPage: function(app){
		return function(req, res) {
		
			var token = req.session.token; // Prende il token dalla sessione
	
			// Controlla che ci sia il token in sessione
			if (token) {
	
				// Verifica la validità del token
				jwt.verify(token, app.get('secretforToken'), function(err, decoded) {			
					if (err) {
						return res.json({ success: false, message: 'Failed to authenticate token.' });		
					} else { // Il token risulta corretto, l'utente viene loggato e visualizza la dashboard
						
						// Vengono caricati tutti gli utenti da mostrare nella dashboard
						UserModel.find({}, function(err, users) {
							
							if ((err)||(!users))
								errorMsg = "Error in retrieving users list!";
							else 
								errorMsg = "";
							
							res.render('dashboard', { // Renderizza la pagina Dashboard per l'utente loggato
								title : 'Magician',
								user: {name: decoded.name, email: decoded.email}, // Passa l'utente loggato
								users: users, // passa la lista degli utenti del sistema da visualizzare
								errorMsg: errorMsg // Passa eventuali errori
							});
						
						});
								
					}
				});
	
			} else {
				// Visulizza la homepage 
				res.render('index', {
					title : 'Magician'
				});
			}
		};
	},
	// Render della pagina di registrazione
	renderRegistration: function(req, res) {
		res.render('registration', {
			title : 'Magician - Registration'
		});
	},
	// Render della pagina di login
	renderLogin: function(req, res) {
		res.render('login', {
			title : 'Magician - Login'
		});
	},
	// Render della pagina di logout
	renderLogout: function(req, res) {
		
		if (req.session.token){ // Se l'utente è loggato 
			req.session.destroy(function(){ // Distrugge la sessione attuale
				res.redirect('/'); // reindirizza alla homepage
			});
		}
		else{ // Se l'utente non è loggato
			res.redirect('/login'); // reindirizza alla pagina del login
		}
	},
	
};

