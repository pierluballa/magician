/**
 * Users controller api
 */

var jwt = require('jsonwebtoken');
var UserModel = require('../models/users_model.js');
var bcrypt = require('bcrypt'); // Libreria per hash della password 

//route middleware per autenticare e controllare il token
var middlewareTokenFunction = function(app, next) { return function(req, res, next) {

	// controlla i parametri di header, l'url  e il parametro in post alla ricerca di un eventuale token
	var token = req.body.token || req.param('token') || req.headers['x-access-token']; 

	// Se il token è presente
	if (token) {

		// Verifica la validità del token
		jwt.verify(token, app.get('secretforToken'), function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });		
			} else {
				//Il token è valido viene eseguita la funzione passata come parametro
				next();
			}
		});

	} else {

		// Se non è inviato alcun token
		// ritorna un errore
		return res.status(403).send({ 
			success: false, 
			message: 'No token provided.'
		});
		
	}
	
}};

module.exports = {
		
	middlewareTokenFunction: middlewareTokenFunction,

	// Route che gestisce l'autenticazione
	// Authenticate prende in input nome utente e password e restituisce un token: input[email, password], output[token]
	authenticate: function (app) {

		return function(req, res){
			
			// Controlla che i parametri in input siano validi e non nulli
			if ((!req.body.email) || (req.body.email == '') || (!req.body.password)
					|| (req.body.password == '')) {
				res.json({
					success : false,
					message : 'Email or password not valid!'
				});
			} else {
		
				// Cerca l'utente
				UserModel.findOne({
					email : req.body.email
				}, function(err, user) {
		
					if (err) {
						return res.json({
							success : false,
							message : 'Error finding User!'
						});
					}
		
					if (!user) { // Se l'utente non è presente ritorna un messaggio di errore
						res.json({
							success : false,
							message : 'Authentication failed. User doesn\'t exists.'
						});
					} else if (user) {
						console.log('validPassword', user.validPassword(req.body.password, user.password));
						
						// Viene controllata la validità della password
						if (user.validPassword(req.body.password, user.password) == false) {
							res.json({
								success : false,
								message : 'Authentication failed. Wrong password.'
							});
						} else {
		
							// L'utente è presente e la password è valida
							// crea il token
							var token = jwt.sign(user, app.get('secretforToken'), {
								expiresInMinutes : app.get('tokenExpTime')
							});
		
							// Il token generato viene messo in sessione	
							req.session.email = req.body.email;
							req.session.token = token;
							
							console.log(token); //TODO: da togliere
								
							// Ritorna il token generato
							res.json({
								success : true,
								message : 'Enjoy your token!',
								token : token
							});
						}
		
					}
		
				});
			}
		}
	},
	//Register prende in input nome utente e password e restituisce success=true nel caso la registrazione sia avvenuta con successo
	register: function(req, res) {

		// Vengono controllati i parametri in input 
		if ((!req.body.email) || (req.body.email === '') || 
				(!req.body.password) || (req.body.password === '')) {
			return res.json({
				success : false,
				message : 'Email or password not valid!'
			});
		}

		// Viene creato l'oggetto User da inserire sul DB
		var user = new UserModel();
		user.set('email', req.body.email);
		user.set('password', user.generateHash(req.body.password));
		if (req.body.name) user.set('name', req.body.name);
		if (req.body.address) user.set('address', req.body.address);
		user.set('isNewsletterSignup', req.body.isNewsletterSignup);
		user.set('isSendNotification', req.body.isSendNotification);
		
		console.log(req.body.email);
		console.log(req.body.password);

		// Salva lo User sul DB
		user.save(function(err) {
			if (err) {
				console.log("Error saving in db: " + err);
				return res.json({
					success : false,
					message : 'This email address has already registered!' //'Error: ' +JSON.stringify(err.message)
				});
			} else {
				
				return res.json({
					success : true,
					message : 'User ' + req.body.email + ' created with success!'
				});
			}
		});

	},
	// Api Delete per la cancellazione di un utente, accesso riservato
	deleteUser: function(req, res) {
		console.log('/api/deleteUser/:email = '+ req.params.email);
		// Vengono controllati i parametri in input
		if ((!req.params.email) || (req.params.email === '')){
			return res.json({
				success : false,
				message : 'Error email user not valid.'
			}); 
		}
		
		// Viene controllata l'esistenza dell'utente da eliminare 
		UserModel.findOne({email: req.params.email }, function(err, user) {
			
			if ((err) || (!user)){
				return res.json({
					success : false,
					message : 'User not found.'
				});
			}
			console.log('user: '+ JSON.stringify(user));
			
			// Viene eliminato l'utente
			user.remove(function(err2) {
			    if (err2) 
				    return res.json({
						success : false,
						message : 'Error removing user.'
					});
			    else
			    	return res.json({
						success : true,
						message : 'User' + user.email + ' removed with success!'
					});

			    console.log('User successfully deleted!');
			  });
			
		});
		
	},
	// Api Delete per la modifica di un utente, accesso riservato
	updateUser: function(req, res) {
		// Vengono controllati i parametri in input
		if ((!req.params.email) || (req.params.email === '')){
			return res.json({
				success : false,
				message : 'Error user email not valid.'
			}); 
		}
		
		var updatedUser = {};
		
		// Vengono registrati gli updates allo user
		if (req.body.password) updatedUser.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
		if (req.body.name) updatedUser.name = req.body.name;
		if (req.body.address) updatedUser.address = req.body.address;
		if (req.body.isNewsletterSignup) updatedUser.isNewsletterSignup = req.body.isNewsletterSignup;
		if (req.body.isSendNotification) updatedUser.isNewsletterSignup = req.body.isSendNotification;
		
		UserModel.findOneAndUpdate({ email: req.params.email }, updatedUser, function(err, user) {
			if ((err) || (!user)){
				return res.json({
					success : false,
					message : 'Error in update.'
				});
			} else {
				return res.json({
					success : true,
					message : 'User ' + user.email + ' updated with success!'
				});
			}

			  // we have the updated user returned to us
			  console.log(user);
		});
		
	},
	// Api per la visualizzazione di tutti gli utenti registrati nel sistema con relative informazioni, accesso riservato
	listUsers: function(req, res) {
		UserModel.find({}, function(err, users) {
			if (err)
				return res.json({
					success : false,
					message : 'Error in listing users.',
					users: null
				});
			res.json({success: true, users: users});
		});
	}
};

